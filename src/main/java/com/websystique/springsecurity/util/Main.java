package com.websystique.springsecurity.util;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
	public static void main(String[] args) throws Exception {
		System.out.println("写Excel");
		 
        List<User> list = new ArrayList<User>();
        User u = new User();
        u.setAge("3");
        u.setName("fdsafdsa");
        u.setXx(123.23D);
        u.setYy(new Date());
        u.setLocked(false);
        u.setDb(new BigDecimal(123));
        list.add(u);
 
        u = new User();
        u.setAge("23");
        u.setName("fdsafdsa");
        u.setXx(123.23D);
        u.setYy(new Date());
        u.setLocked(true);
        u.setDb(new BigDecimal(234));
        list.add(u);
 
        u = new User();
        u.setAge("123");
        u.setName("fdsafdsa");
        u.setXx(123.23D);
        u.setYy(new Date());
        u.setLocked(false);
        u.setDb(new BigDecimal(2344));
        list.add(u);
 
        u = new User();
        u.setAge("22");
        u.setName("fdsafdsa");
        u.setXx(123.23D);
        u.setYy(new Date());
        u.setLocked(true);
        u.setDb(new BigDecimal(908));
        list.add(u);
 
        ExcelDataFormatter edf = new ExcelDataFormatter();
        Map<String, String> map = new HashMap<String, String>();
        map.put("真", "true");
        map.put("假", "false");
        edf.set("locked", map);
 
        ExcelUtils.writeToFile(list,edf, "Countries.xlsx");
 
        List<User> xx = new ExcelUtils<User>(new User()).readFromFile(edf, new File("Countries.xlsx"));
        for (User user : xx) {
			System.out.println(user);
		}
	}
}
